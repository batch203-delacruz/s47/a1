// console.log("Hello World!")

// DOCUMENT OBJECT MODEL
	// -get, change, add, or delete HTML elements

// Selecting HTML elements - document.querySelector("htmlElement")

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
const spanColor = document.querySelector("#text-color");



// EVENT LISTENERS

// addEventListener 
txtFirstName.addEventListener("keyup", (event) => {
	console.log(event.target); //contains the element
	console.log(event.target.value); // actual value
});

txtFirstName.addEventListener("keyup", (e) => {
	// innerHTML sets or returns the HTML content (innerHTML) of an element (div, span, etc)
	// .value sets or returns the value of an attribute
	spanFullName.innerHTML = `${txtFirstName.value}`;
});

// Multiple events that will trigger the same function

const fullName = () => {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);


//Activity s47
    /*
    1. Create an addEventListener that will "change the color" of the "spanFullName". 
    2. Add a "select tag" element with the options/values of red, green, and blue in your index.html.
    3. The values of the select tag will be use to change the color of the span with the id "spanFullName"
*/

spanColor.addEventListener("click", (event) => {
	spanFullName.style.color = spanColor.value;
})
